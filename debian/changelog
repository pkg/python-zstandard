python-zstandard (0.20.0-3+apertis0) apertis; urgency=medium

  * Import from Debian bookworm.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Tue, 25 Apr 2023 15:18:26 +0530

python-zstandard (0.20.0-3) unstable; urgency=high

  * debian/control: Also add dependency on libzstd1 (<< 1.5.5~).
    (Closes: #1032031)

 -- Boyuan Yang <byang@debian.org>  Mon, 27 Feb 2023 11:43:22 -0500

python-zstandard (0.20.0-2) unstable; urgency=high

  * debian/control: Explicitly depends on libzstd1 (>= 1.5.4~)
    to circumvent autopkgtest errors.

 -- Boyuan Yang <byang@debian.org>  Tue, 21 Feb 2023 16:15:42 -0500

python-zstandard (0.20.0-1) unstable; urgency=high

  * New upstream release.
    + Fix compatibility with libzstd 1.5.4. (Closes: #1031293)
  * debian/control: Explicitly require libzstd-dev (>= 1.5.4~).

 -- Boyuan Yang <byang@debian.org>  Mon, 20 Feb 2023 17:29:29 -0500

python-zstandard (0.19.0-3) unstable; urgency=medium

  * Team upload.
  * d/control: depend on python3-exceptiongroup.
    This allows us to restore support for python3.10 a little longer,
    and also smoothens transition to testing with existing reverse
    dependencies.
    Thanks to s3v <c0llapsed@yahoo.it>
  * d/control: declare compliance to standards version 4.6.2.
  * d/control: declare rules do not require root.

 -- Étienne Mollier <emollier@debian.org>  Wed, 25 Jan 2023 20:18:20 +0100

python-zstandard (0.19.0-2) unstable; urgency=medium

  * Team upload.
  * d/control: build depends only on python3-dev. (Closes: #1028837)
  * d/control: activate autodep8 python tests.

 -- Étienne Mollier <emollier@debian.org>  Sat, 21 Jan 2023 18:36:31 +0100

python-zstandard (0.19.0-1) unstable; urgency=medium

  * New upstream release.

 -- Boyuan Yang <byang@debian.org>  Tue, 01 Nov 2022 13:48:07 -0400

python-zstandard (0.18.0-2) unstable; urgency=medium

  * Source-only upload.

 -- Boyuan Yang <byang@debian.org>  Fri, 08 Jul 2022 12:00:28 -0400

python-zstandard (0.18.0-1) unstable; urgency=medium

  * Initial release.
  * d/copyright: Update information.

 -- Boyuan Yang <byang@debian.org>  Wed, 06 Jul 2022 17:59:09 -0400
